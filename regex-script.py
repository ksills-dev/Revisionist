#!/bin/python

import re
import sys
from random import randint

filename = sys.argv[1]
if (filename == None):
    exit('I need an input file!')

print("Generating dataset from " + filename + ".json")

input_file = open(filename + '.json', 'r')
output_file = open(filename + '.out', 'w')

regex = re.compile("""\[\{\"ty\":\"is\",\"s\":\"(.*?)\",\"ibi\":\d+\},(\d+),.*?\]""")

print("Matching...")
report_list = []
for match in regex.findall(input_file.read()):
    report_list.append((match[0], int(match[1])))

print("Generating Differences...")
difference_list = []
for index in range(0, len(report_list) - 1):
    report = report_list[index]
    chars = report[0]
    time = report[1]

    difference = report_list[index + 1][1] - time
    difference_split = int(difference / len(chars))
    difference_variance = int(difference / len(chars * 20))

    for char in chars:
        difference_adjust = randint(-1 * difference_variance, difference_variance)
        difference_list.append(difference_split + difference_adjust)

print("Running Analytics...")
average = 0
count = 0
for difference in difference_list:
    average = average + difference
    count = count + 1
average = int(average / count)

print("Writing output to " + filename + ".out...")
for difference in difference_list:
    output_file.write(str(difference) + '\n')

output_file.write("\n")
output_file.write("Keys Pressed: " + str(count + 1) + "\n")
output_file.write("Average Keystroke :" + str(average) + "ms" + "\n")
output_file.write("WPM: " + str(int((60 * 1000) / (average * 5.1))) + "\n")

print("Done!")