# Revisionist
A quick and dirty script to help my sister with a school project.
She needed to fetch the Google revision history for documents typed start to
finish. Then with that data she needed to determine the total number of keys 
pressed, the average time (in ms) for each keystroke and the resulting words
per minute (WPM).

## Fetching a Revision List
Using the [Google API](https://developers.google.com/drive/api/v3/reference/revisions/list)
download the first and last revision IDs for a document. Then the revision 
history can be fetched in JSON format at 
https://docs.google.com/document/d/**<fileid>**/revisions/load?id=**<fileid>**&start=**<revision1>**&end=**<revision2>**.

## Parsing the Revision Format
Under the time constraints I didn't have much of a chance to reverse engineer 
the whole format (nor did I find any documentation on it from a quick
Google-Fu). However I was able to parse this much:

```
[{"op": subop , ? : data , ? : ?}, time, someID, revision, someID, (revision - 2?), null]
```

Where:

* op = operation performed.
* subop = more specific operation information.
* ? = I have no idea.
* data = content of the operation.
* time = time (not sure what precision). Likely from Unix epoch (will help you find precision).
* revision = revision number
* someID = An ID that stayed the same in every single revision I made. One of them is likely the user ID.
* null = literally `null` every time from what I see.

There may be multiple `{}` blocks in a single revision. Not sure what determines 
if they get grouped, though.

An operation "ty" seems to be something being typed. subop "is" is for 
insertions. "ds" is for deletions. For inserts the data is the letters typed. 
For deletions it's some number - perhaps probably some index.

This is everything I needed for the purposes of her project.

## Running the script
Just run `./regex-script.py <filename>` (elide the file extension).

The program will output a file with the same name but with the suffix `.out`
containing the analytics we need.